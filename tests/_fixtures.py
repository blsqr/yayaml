"""Test utilities, fixtures, ..."""

import os
import pathlib
import shutil

import pytest

from . import (
    ABBREVIATE_TEST_OUTPUT_DIR,
    TEST_OUTPUT_DIR,
    TEST_VERBOSITY,
    USE_TEST_OUTPUT_DIR,
)

# -----------------------------------------------------------------------------
# Output Directory


@pytest.fixture
def tmpdir_or_local_dir(tmpdir, request) -> pathlib.Path:
    """If ``USE_TEST_OUTPUT_DIR`` is False, returns a temporary directory;
    otherwise a test-specific local directory within ``TEST_OUTPUT_DIR`` is
    returned.
    """
    if not USE_TEST_OUTPUT_DIR:
        return tmpdir

    if not ABBREVIATE_TEST_OUTPUT_DIR:
        # include the module and don't do any string replacements
        test_dir = os.path.join(
            TEST_OUTPUT_DIR,
            request.node.module.__name__,
            request.node.originalname,
        )
    else:
        # generate a shorter version without the module and with the test
        # prefixes dropped
        test_dir = os.path.join(
            TEST_OUTPUT_DIR,
            request.node.originalname.replace("test_", ""),
        )

    # Clean out that directory and then recreate it
    print(f"Using local test output directory:\n  {test_dir}")
    if os.path.isdir(test_dir):
        shutil.rmtree(test_dir)
    os.makedirs(test_dir, exist_ok=True)

    return pathlib.Path(test_dir)


out_dir = tmpdir_or_local_dir
"""Alias for ``tmpdir_or_local_dir`` fixture"""


# -----------------------------------------------------------------------------
# -- Test data creation -------------------------------------------------------
# -----------------------------------------------------------------------------
