.. _welcome:

``yayaml``
==========

🚧 **WORK IN PROGRESS** 🚧

Sorry, there's not a lot to see here yet apart from the :py:mod:`yayaml` API documentation.
In the meantime, have a look at the `yayaml project page README <https://gitlab.com/blsqr/yayaml>`_ which has some usage examples.

:py:mod:`yayaml` is used in the following projects to read and write YAML
files, including custom constructors and representers:

* `paramspace <https://gitlab.com/blsqr/paramspace>`_: for config-file-based
  grid search with deeply nested dicts
* `dantro <https://gitlab.com/utopia-project/dantro>`_: for loading, processing
  and visualizing high-dimensional simulation output
* `utopya <https://gitlab.com/utopia-project/utopya>`_: a versatile simulation
  framework


.. note::

    If you find any errors in this documentation or would like to contribute to the project, we are happy about your visit to the `project page <https://gitlab.com/blsqr/yayaml>`_.


.. toctree::
    :hidden:

    Repository <https://gitlab.com/blsqr/yayaml>
    Changelog <https://gitlab.com/blsqr/yayaml/-/blob/main/CHANGELOG.md>

.. toctree::
    :caption: Reference
    :maxdepth: 2
    :hidden:

    API Reference <api/yayaml>
    index_pages
